// Autor: Marcos Moreno

#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <fcntl.h>


int main(int argc,char* argv[])
{
	int fd = mkfifo("TuberiaLogger", 0640);	
	if(fd==-1)
	{	
		perror("mkfifo()");
		return -1;
	}	

	char buf[100];
	int n;

	int tuberia = open("TuberiaLogger", O_RDONLY);
	if(tuberia==-1)
	{
		perror("open()");
		return -1;
	}
	
	while(1)
	{
		n = read(tuberia, buf, sizeof(char)*100);
		if(n>0)	
			if(buf[0]=='c')	//si recibe una c, sale del bucle.
				break;
			else
				puts(buf);
		else if(n==0)
			break;
	}

	close(tuberia);

	int b=unlink("TuberiaLogger");
	if(b==-1)
	{
		perror("unlink()");
		return -1;
	}
	
	return 0;
}
