#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <fcntl.h>
#include <sys/mman.h>
#include <sys/types.h>

#include "DatosMemCompartida.h"

int main(void)
{
	DatosMemCompartida * pdatos;
	
	//Abrir fichero y proyectarlo en memoria

	int fd = open("DatosMemoriaCompartida.txt", O_RDWR, 0640);
	if(fd==-1)
	{	
		perror("open()");
		return -1;
	}
	
	pdatos = (DatosMemCompartida *) mmap(NULL, sizeof(DatosMemCompartida), PROT_READ | PROT_WRITE, MAP_SHARED, fd, 0);
	if(pdatos==MAP_FAILED)
	{	
		perror("mmap()");
		return -1;
	}
	close(fd);

	while(1)
	{
		//lógica de la inteligencia artificial
		if (pdatos->esfera.centro.y > pdatos->raqueta1.y2)
			pdatos->accion=1;//Si la esfera está por encima de la raqueta, manda la acción subir.
		else if (pdatos->esfera.centro.y < pdatos->raqueta1.y1)
			pdatos->accion=-1;//Si la esfera está por debajo de la raqueta, manda la acción bajar.
		else
			pdatos->accion=0;//Si la esfera está a la altura de la raqueta, manda la acción nada.
	
		usleep(25000);
	}
}
